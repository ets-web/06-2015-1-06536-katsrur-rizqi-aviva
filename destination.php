<!DOCTYPE html>
	<head>
		<title>Belajar HTML dan CSS</title>
		<link rel="stylesheet" href="destinationstyle.css"/>
	</head>
	<body>
		<div class="Badan">
			<div class="header">
				<div class="logo">
					<div class="inlogo">
						<img src="logo.jpg">
					</div>
					<div class="inlogo2">
						<h2>KarizaTravel.id</h2>
					</div>
				</div>
				<div class="menu">
					<div class="portfolio">
						<a href="portfolio.php"><h3>PORTFOLIO</h3></a>
					</div>
					<div class="portfolio">
						<a href="about.php"><h3>ABOUT</h3></a>
					</div>
					<div class="portfolio">
						<a href="contact.php"><h3>CONTACT</h3></a>
					</div>
					<div class="portfolio">
						<a href="destination.php"><h3>DESTINATION</h3></a>
					</div>
					<div class="portfolio">
						<a href="index.php"><h3>HOME</h3></a>
					</div>
				</div>
			</div>
			
				<div class="tempport">
				<div class="tempportone">
					<div class="portone">
						<img src="hotel1.jpg"> 
					</div>
					<div class="porttwo">
						<img src="hotel2.jpg">
					</div>
					<div class="portthree">
						<img src="hotel3.jpg"> 
					</div>
				</div>
				<div class="empporttwo">
					<div class="underportone">
						<h2>Hotel Mawar<br>
								jalan Dinoyo no 17<br>
								hub:085755376543</h2>
					</div>
					<div class="underporttwo">
						<h2>Hotel Melati<br>
								Klampis Aji no 5 <br>
								hub:083755679643</h2>
					</div>
					<div class="underportthree">
						<h2>Hotel sunshine<br>
								jalan Indra Pura no 100<br>
								hub:085987676543</h2>
					</div>
				</div>
			</div>
			
<br><br><br><div class="tempportfolio">
				<div class="tempportfolioone">
					<div class="oneport">
						<img src="hotel4.jpg"> 
						
					</div>
					<div class="twoport">
						<img src="hotel5.jpg"> 
						
					</div>
					<div class="threeport">
						<img src="hotel6.jpg">
						
					</div>
				</div class="tempportfoliotwo">
				<div>
					<div class="underoneport">
						<h2>Hotel Moon Hotel<br>
							Siwalankerto no 89<br>
							hub:085756987156</h2>
					</div>
					<div class="undertwoport">
						<h2>Hotel Fantasi Hotel<br>
							Jalan Pahlawan no 70<br>
							hub:0819865678142</h2>
					</div>
					<div class="underthreeport">
						<h2>Hotel Sealow Hotel<br>
							Jalan Ir. Soekarno no 105<br>
							hub:085789653456</h2>
					</div>
				</div>
			</div>
			
		</div>
			
		</div>
	</body>
	
</html>